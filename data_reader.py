import re
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from pathlib import Path
from typing import Optional

import geopandas as gpd


def check_ext(file_path: Path, ext: str) -> None:
    """Function to validate extension of file with user defined ext."""
    if file_path.suffix != ext:
        raise ExtensionError(
            ext,
            f"Incorrect extension of input_file: "
            f"{file_path.name}, should be ext: *{ext}",
        )


def check_epsg(crs: str) -> None:
    """Function to check if input crs is in format 'EPSG:XXXX'."""
    if re.search("EPSG:\d{4}", crs) is None:
        raise EPSGError(
            crs,
            f"Incorrect format of coordinate system: {crs},\n"
            f'crs attribute should be in format "EPSG:XXXX"',
        )


def read_geofile(file_path: Path, crs: str, **kwargs) -> gpd.GeoDataFrame:
    """Function to read all geodata formats into GeoDataFrame with optional parameters."""
    try:
        data = gpd.read_file(filename=file_path, crs=crs, **kwargs)
    except Exception as e:
        e.add_note(f"Problem with input file: {file_path}")
        raise
    return data


def category2str(data_frame: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """Convert categorical columns into strings"""
    category_list: list = [
        key
        for key in dict(data_frame.dtypes)
        if dict(data_frame.dtypes)[key] in ["category"]
    ]
    data_frame[category_list] = data_frame[category_list].astype(str)

    return data_frame


class ExtensionError(ValueError):
    def __init__(self, ext, message) -> None:
        self.ext = ext
        self.message = message
        super().__init__(self.message)


class EPSGError(ValueError):
    def __init__(self, ext, message) -> None:
        self.ext = ext
        self.message = message
        super().__init__(self.message)


class Reader(ABC):
    """Abstract of all Geo-Readers"""

    def __init__(self):
        self.gdf: gpd.GeoDataFrame = gpd.GeoDataFrame()

    @abstractmethod
    def __post_init__(self):
        """Validate input"""

    @abstractmethod
    def read(self) -> None:
        """Read data"""

    @property  # Return GeoDataFrame with categorical columns
    def data(self):
        return self.gdf

    @property  # Return GeoDataFrame with str columns
    def str_data(self):
        return category2str(self.gdf)


@dataclass
class ShpReader(Reader):
    file_path: Path
    crs: str

    def __post_init__(self):
        # * Validation
        check_ext(self.file_path, ".shp")
        check_epsg(self.crs)

        # * Read data
        self.read()

    def read(self) -> None:
        self.gdf = read_geofile(self.file_path, crs=self.crs)
        self.gdf = gpd.GeoDataFrame(self.gdf.set_index("index"))


@dataclass
class GpkgReader(Reader):
    file_path: Path
    layer: str
    crs: str
    schema: Optional[dict[str, str]] = field(default_factory=dict)

    def __post_init__(self):
        # * Validation
        check_ext(self.file_path, ".gpkg")
        check_epsg(self.crs)

        # * Finally try read data and preprocess
        self.read()
        if self.schema:
            self.preprocess()

    def read(self) -> None:
        self.gdf = read_geofile(self.file_path, crs=self.crs, layer=self.layer)

    def preprocess(self) -> None:
        try:
            self.gdf = gpd.GeoDataFrame(self.gdf.astype(self.schema))
        except KeyError as e:
            print(f"Invalid schema in file: '{self.file_path.name}'\n")
            raise
